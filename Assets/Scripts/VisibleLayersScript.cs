﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisibleLayersScript : MonoBehaviour {

	Camera cam;
	public GameObject Objects;
	public GameObject Ground;
	private GameObject[] PlayerTag;

	// Use this for initialization
	void Start () {
		PlayerTag = GameObject.FindGameObjectsWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.A)){
			Debug.Log("pressed a");
			Camera.main.cullingMask = 1 << 8;
		}
		if (Input.GetKey(KeyCode.B)){
			Debug.Log("pressed B");
			Camera.main.cullingMask = 1 << 9;
		}
		if (Input.GetKey(KeyCode.C)){
			Objects.layer = 8;
			foreach(Transform trans in Objects.transform){
				trans.gameObject.layer = 0;
			}
			Ground.layer = 9;
			foreach(Transform trans in Ground.transform){
				trans.gameObject.layer = 0;
			}
			Camera.main.cullingMask = 1;
		}
		if (Input.GetKey(KeyCode.Equals)){
			foreach (GameObject playtag in PlayerTag){
				playtag.transform.localScale += new Vector3(0.15f,0.15f,0.15f)*Time.deltaTime;
			}
		}
		if (Input.GetKey(KeyCode.Minus)){
			foreach (GameObject playtag in PlayerTag){
				playtag.transform.localScale -= new Vector3(0.15f,0.15f,0.15f)*Time.deltaTime;
			}	
		}
	}
}
