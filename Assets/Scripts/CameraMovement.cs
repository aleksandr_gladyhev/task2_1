﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

float step = 0.25f;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("up")){
			gameObject.transform.position += gameObject.transform.forward * step;
		}
		if (Input.GetKeyDown("down")){
			gameObject.transform.position -= gameObject.transform.forward * step;
		}
		if (Input.GetKeyDown("right")){
			gameObject.transform.position += gameObject.transform.right * step;
		}
		if (Input.GetKeyDown("left")){
			gameObject.transform.position -= gameObject.transform.right * step;
		}
	}
}
